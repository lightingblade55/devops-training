#!/bin/bash

while true; 
do
	if [ -d "/home/duynguyen/FioTest" ] 
	then
		vmstat 1 10  >> /home/duynguyen/FioTest/$(date +%F_%T |tr ":" "-")-vmstat;
		iostat -dmx 1 10  >> /home/duynguyen/FioTest/$(date +%F_%T |tr ":" "-")-iostat;
		mpstat -P ALL 1 10 >> /home/duynguyen/FioTest/$(date +%F_%T |tr ":" "-")-mpstat;
		break
	fi
	echo "Folder exists and statfiles were created!"
done
echo "Done!"
