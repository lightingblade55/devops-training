set bg=dark ai et sw=2 ts=2
nmap <C-n> :set invnumber<CR>
abbr _SH #!/bin/bash
syntax on
autocmd Filetype yaml setlocal ai ts=2 sw=2 et nu cuc cul